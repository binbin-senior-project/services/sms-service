package repository

import (
	"database/sql"
)

// ISMSRepository interface
type ISMSRepository interface{}

// SMSRepository struct
type SMSRepository struct {
	db *sql.DB
}

// NewSMSRepository method
func NewSMSRepository(db *sql.DB) ISMSRepository {
	return &SMSRepository{
		db: db,
	}
}
