package amqp

import (
	"log"
	"os"

	amqp "github.com/streadway/amqp"
)

// NewNotificationDelivery method is an intial grpc package
// to start gRPC transportation
func NewNotificationDelivery() *amqp.Channel {
	var (
		rmqHost = os.Getenv("RABBITMQ_HOST")
		rmqUser = os.Getenv("RABBITMQ_USER")
		rmqPass = os.Getenv("RABBITMQ_PASSWORD")
		rmqPort = os.Getenv("RABBITMQ_PORT")
	)

	conn, err := amqp.Dial("amqp://" + rmqUser + ":" + rmqPass + "@" + rmqHost + ":" + rmqPort)
	failOnError(err, "Failed to connect to RabbitMQ")

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	return ch
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
