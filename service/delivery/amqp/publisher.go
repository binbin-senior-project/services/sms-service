package amqp

import (
	amqp "github.com/streadway/amqp"
)

// AMQPPublish Holayy!
type AMQPPublish struct {
	channel *amqp.Channel
}

// NewAMQPPublish constructor
func NewAMQPPublish(channel *amqp.Channel) *AMQPPublish {
	return &AMQPPublish{
		channel: channel,
	}
}
