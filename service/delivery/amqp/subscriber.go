package amqp

import (
	amqp "github.com/streadway/amqp"
	u "gitlab.com/mangbinbin/services/notification-service/service/usecase"
)

// AMQPSubscribe struct
type AMQPSubscribe struct {
	usecase u.RegisterUsecase
	channel *amqp.Channel
}

// NewAMQPSubscribe method
func NewAMQPSubscribe(usecase u.RegisterUsecase, channel *amqp.Channel) {}
