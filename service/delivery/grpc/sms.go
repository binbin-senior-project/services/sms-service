package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/notification-service/service/delivery/grpc/proto"
)

// RequestOTP method
func (d *NotificationDelivery) RequestOTP(ctx context.Context, req *pb.RequestOTPRequest) (*pb.RequestOTPReply, error) {
	// Create trash by usecase
	phone := req.Phone

	//  Send OTP to user via usecase
	go d.usecase.SMS.RequestOTP(phone)

	return &pb.RequestOTPReply{Success: true}, nil
}

// VerifyOTP method
func (d *NotificationDelivery) VerifyOTP(ctx context.Context, req *pb.VerifyOTPRequest) (*pb.VerifyOTPReply, error) {
	// Create trash by usecase
	phone := req.Phone
	code := req.Code

	//  Send OTP to user via usecase
	success, err := d.usecase.SMS.VerifyOTP(phone, code)

	if err != nil {
		return &pb.VerifyOTPReply{Success: false}, err
	}

	return &pb.VerifyOTPReply{Success: success}, nil
}
