package grpc

import (
	"log"
	"net"
	"os"

	amqp "gitlab.com/mangbinbin/services/notification-service/service/delivery/amqp"
	pb "gitlab.com/mangbinbin/services/notification-service/service/delivery/grpc/proto"
	u "gitlab.com/mangbinbin/services/notification-service/service/usecase"
	"google.golang.org/grpc"
)

// NotificationDelivery Holayy!
type NotificationDelivery struct {
	broker  *amqp.AMQPPublish
	usecase u.RegisterUsecase
}

// NewNotificationDelivery method is an intial grpc package
// to start gRPC transportation
func NewNotificationDelivery(broker *amqp.AMQPPublish, usecase u.RegisterUsecase) {
	// Register protobuf handler with AuthgRPCDelievery struct
	delivery := &NotificationDelivery{
		broker:  broker,
		usecase: usecase,
	}

	port := os.Getenv("SERVICE_PORT")

	// Listen out
	log.Println("Listen on port: " + port)

	// Create TCP Connection
	lis, err := net.Listen("tcp", ":"+port)

	// Detect connection error
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create GRPC service
	s := grpc.NewServer()

	// Register GRPC to protobuffer
	pb.RegisterSMSServiceServer(s, delivery)

	// Register TCP connection to GRPC
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
