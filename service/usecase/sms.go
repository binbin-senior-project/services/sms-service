package usecase

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"

	r "gitlab.com/mangbinbin/services/notification-service/service/repository"
)

// ISMSUsecase interface
type ISMSUsecase interface {
	RequestOTP(phone string) error
	VerifyOTP(phone string, code string) (bool, error)
}

// SMSUsecase is a struct
type SMSUsecase struct {
	repo r.RegisterRepository
}

// NewSMSUsecase method
func NewSMSUsecase(repo r.RegisterRepository) ISMSUsecase {
	return &SMSUsecase{
		repo: repo,
	}
}

// RequestOTP method is a method used by Binbin user application
// To confirm the trash is an exisiting trash
// when user throwing a bottle into a bin
func (u *SMSUsecase) RequestOTP(phone string) error {

	// Declare needed value from .env file
	var (
		authyAPIKey = os.Getenv("AUTHY_API_KEY")
		countryCode = "66"
	)

	// Declare Twillio verify endpoint
	endpoint := "https://api.authy.com/protected/json/phones/verification/start"

	// Set data value as a params mapping
	// It is supported only "Thai" phone
	data := url.Values{}
	data.Set("phone_number", phone)
	data.Set("via", "sms")
	data.Set("country_code", countryCode)
	data.Set("code_length", "6")
	data.Set("locale", "th")

	// Encode the data as string
	dataReader := *strings.NewReader(data.Encode())

	// Create client request with POST
	// and declare data as FORM
	client := &http.Client{}
	req, _ := http.NewRequest("POST", endpoint, &dataReader)
	req.Header.Add("X-Authy-API-Key", authyAPIKey)

	// Make a request
	resp, err := client.Do(req)

	fmt.Println(resp)

	defer resp.Body.Close()

	// If Status code in range of 200 and 300
	// It is satified by Twillio of success
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		// Declare data to receive decoded data
		var data map[string]interface{}
		// Declare decoded for decode from response
		decoder := json.NewDecoder(resp.Body)
		// Decode json from response
		err = decoder.Decode(&data)

		// Return success value
		if err == nil {
			return nil
		}
	}

	// Return fail value
	return err
}

// VerifyOTP method is a method used by Binbin user application
// To confirm the trash is an exisiting trash
// when user throwing a bottle into a bin
func (u *SMSUsecase) VerifyOTP(phone string, code string) (bool, error) {
	// Declare needed value from .env file
	var (
		authyAPIKey = os.Getenv("AUTHY_API_KEY")
		countryCode = "66"
	)

	// Declare Twillio verify endpoint
	url := "https://api.authy.com/protected/json/phones/verification/check"
	endpoint := url + "?phone_number=" + phone + "&country_code=" + countryCode + "&verification_code=" + code

	// Create client request with POST
	// and declare data as FORM
	client := &http.Client{}
	req, _ := http.NewRequest("GET", endpoint, nil)
	req.Header.Add("X-Authy-API-Key", authyAPIKey)

	// Make a request
	resp, err := client.Do(req)

	fmt.Println(resp)

	if err != nil {
		return false, err
	}

	defer resp.Body.Close()
	// If Status code in range of 200 and 300
	// It is satified by Twillio of success
	if resp.StatusCode >= 200 && resp.StatusCode < 300 {
		// Declare data to receive decoded data
		var data map[string]interface{}
		// Declare decoded for decode from response
		decoder := json.NewDecoder(resp.Body)
		// Decode json from response
		err = decoder.Decode(&data)

		// Return success value
		if err == nil {
			return true, nil
		}
	}

	// Return fail value
	return false, nil
}
