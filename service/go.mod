module gitlab.com/mangbinbin/services/notification-service/service

go 1.13

require (
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/appleboy/go-fcm v0.1.5
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/golang/protobuf v1.3.3
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.4.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	google.golang.org/grpc v1.27.1
	mellium.im/sasl v0.2.1 // indirect
)
